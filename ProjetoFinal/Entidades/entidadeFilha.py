from Entidades.entidadeMae import Pessoa

class Estudante(Pessoa):
    def __init__(self,ra=""):
        super().__init__()
        self._ra = ra

    @property
    def ra(self):
        return self._ra

    @ra.setter
    def ra(self,ra):
        self._ra = ra

    def email(self):
        print(self._nome.lower() + '.' + self._sobrenome.lower() + '@sempreceub.com')

    def __str__(self):
        return '''
        ---- Estudante ----
        Nome: {}
        Sobrenome: {}
        Idade: {}
        CPF: {}
        RA: {}
        '''.format(self.nome,self.sobrenome,self.idade,self.cpf,self.ra)
