import re

class Validador:
    
    @staticmethod
    def validar(expReg,msgInvalido, msgValido):
        teste = False
        while teste == False:
            valor = input("\nInforme um valor: ")
            verificarEx = re.match(expReg,valor)
            if verificarEx == None:
                print(msgInvalido.format(expReg))
            else:
                print(msgValido.format(valor))
                return valor

    @staticmethod
    def verificarInteiro():
        teste = False
        while teste == False:
            valor = input("Informe um inteiro: ")
            v = re.match("\d+",valor)
            if v != None:
                teste = True
        return int(valor)

    @staticmethod
    def validarValorInformado(valorAtual,textoMsg):
        novoValor = input(textoMsg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual


        
