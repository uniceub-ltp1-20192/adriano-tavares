class Person:
    def __init__(self,fname,lname,height,age):
        self.fname = fname
        self.lname = lname
        self.height = height
        self.email = self.fname + "." + self.lname + "@gmail.com"
        self.age = age

    def introduce(self):
        print("Hello, my name is",self.fname,self.lname)


class Cachorro:
    def __init__(self,tamanho,raca,cor,peso):
        self._tamanho = tamanho
        self._raca = raca
        self._cor = cor
        self._peso = peso

    @property
    def tamanho(self):
        return self._tamanho
    @tamanho.setter
    def tamanho(self,tamanho):
        self._tamanho = tamanho

    @property
    def raca(self):
        return self._raca
    @raca.setter
    def raca(self,raca):
        self._raca = raca

    @property
    def cor(self):
        return self._cor
    @cor.setter
    def cor(self,cor):
        self._cor = cor

    @property
    def peso(self):
        return self._peso
    @peso.setter
    def peso(self,peso):
        self._peso = peso

    def latir(self):
        print("Au Au")

    def comer(self):
        print("Estou comendo")
