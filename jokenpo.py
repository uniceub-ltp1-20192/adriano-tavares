from random import randint

def ask(text):
    return input(text)

def cpuGenerator():
    r = randint(1,3)
    if r == 1:
        return "pedra"
    if r == 2:
        return "papel"
    return "tesoura"

def result(player,cpu,name):
    if player == cpu:
        return "EMPATE!"
    
    if (player == "pedra" and cpu == "tesoura") or (player == "papel" and cpu == "pedra") or (player == "tesoura" and cpu == "papel"):
        return f"{name} venceu!"
    
    return "CPU venceu!"

def main():
    print("|||||||| PEDRA PAPEL E TESOURA ||||||||")

    name = ask("\nQual o seu nome? ")
    player = ask("Pedra Papel ou tesoura? ").lower()
    
    cpu = cpuGenerator()
    print(f"\n{name}: {player}\nCPU: {cpu}\n")
    print(result(player,cpu,name))


main()
