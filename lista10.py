#10.1
for i in range(1,21):
    print(i,end=" ")
print("\n")

#10.2
lista = [valor for valor in range(1,1000001)]
"""print(lista)"""

#10.3
print("MENOR:",min(lista))
print("MAIOR:",max(lista))
print("SOMA:",sum(lista))
print("")

#10.4
lista = [valor for valor in range(1,21,2)]
for elemento in lista:
    print(elemento,end=" ")
print("\n")

#10.5
lista = [valor for valor in range(3,1001,3)]
for elemento in lista:
    print(elemento,end=" ")
print("\n")

#10.6
lista = []
for i in range(1,101):
    lista.append(i**3)
for elemento in lista:
    print(elemento,end=" ")
print("\n")

#10.7
lista = [valor**3 for valor in range(1,101)]
for elemento in lista:
    print(elemento,end=" ")
