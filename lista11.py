#11.1
convidados = ["Chester Bennington","Oliver Sykes","Pete Wentz","Sam Carter","Anthony Green","Mark Hoppus","Brendon Urie"]
print("Os primeiros 3 convidados são:")
i = 0
for convidado in convidados[0:3]:
    print(convidado)

print("Os últimos 3 convidados são:")
for convidado in convidados[-3:]:
    print(convidado)

print("Os convidados do meio são:")
for convidado in convidados[ (len(convidados)//2)-1 : (len(convidados)//2)+2 ]:
    print(convidado)

#11.2
pizza_hut = ["mussarela","calabresa","catupiri","portuguesa","chocolate"]
pizza_hut_paraguai = pizza_hut[:]

pizza_hut.append("sorvete")
pizza_hut_paraguai.append("abacaxi")
print("\nPIZZA HUT:",pizza_hut)
print("PIZZA HUT PARAGUAI:",pizza_hut_paraguai)

#11.3
print("\n||||||CARDÁPIO DE PIZZAS||||||")
i = 1
for pizza in pizza_hut:
    print(f"{i} - {pizza.title()}")
    i += 1
