#12.1
comidas = ("pizza","macarrão","carne","hambúrguer","pipoca")
for comida in comidas:
    print(comida)

print("")

comidas = ("pizza","macarrão","cachorro quente","hambúrguer","batata frita")
for comida in comidas:
    print(comida)

print("")
try:
    comidas[0] = "cachorro quente"
except TypeError:
    print("Não se pode alterar uma tupla")
