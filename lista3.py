#intro1
name = "cebola roxa"
print(name.title())

#intro2
print(name.lower())
print(name.upper())

#3.1
quantidade_cebolas = 5
preco_cebola = 3
print("\nVocê irá pagar " + str(quantidade_cebolas*preco_cebola) + " reais pelas cebolas.\n")

#3.2
cebolas = 300  #atribui o valor 300 à variável cebolas
cebolas_na_caixa = 120  #atribui o valor 120 à variável cebolas_na_caixa
espaco_caixa = 5  #atribui o valor 5 à variável espaco_caixa
caixas = 60  #atribui o valor 60 à variável caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa  #as cebolas fora da caixa são as que não estão na caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)  #as caixas vazias são as que não possuem cebolas
caixas_necessarias = cebolas_fora_da_caixa/espaco_caixa  #as caixas necessárias são dadas por quantas cebolas cabem no espaço de cada caixa
print("Existem",cebolas_na_caixa,"cebolas encaixotadas")
print("Existem",cebolas_fora_da_caixa,"cebolas sem caixa")
print("Em cada caixa cabem",espaco_caixa,"cebolas")
print("Ainda temos",int(caixas_vazias),"caixas vazias")
print("Então, precisamos de",int(caixas_necessarias),"caixas para empacotar todas as cebolas\n")

#3.3
cebolas = 300  
cebolas_na_caixa = 120  
espaco_caixa = 7  #mudança na variável
caixas = 60  
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa  
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)  
caixas_necessarias = cebolas_fora_da_caixa/espaco_caixa  
print("Existem",cebolas_na_caixa,"cebolas encaixotadas")
print("Existem",cebolas_fora_da_caixa,"cebolas sem caixa")
print("Em cada caixa cabem",espaco_caixa,"cebolas")
print("Ainda temos",caixas_vazias,"caixas vazias")  #valor não é inteiro
print("Então, precisamos de",caixas_necessarias,"caixas para empacotar todas as cebolas")  #valor não e inteiro
