#5.1
num = int(input("Digite um número: "))
if num % 2 == 0:
    print("O número é par")
else:
    print("O número é ímpar")

#5.2
num = int(input("\nDigite um número: "))
divisores = 0
for i in range(1,num+1):
    if num % i == 0:
        divisores += 1
if divisores == 2:
    print("O número é primo")
else:
    print("O número não é primo")

#5.3
num1 = int(input("\nDigite um número: "))
num2 = int(input("Digite outro número: "))
print("Soma:",num1+num2)
print("Subtração:",num1-num2)
print("Multiplicação:",num1*num2)
print("Divisão:",num1/num2)

#5.4
idade = int(input("\nDigite sua idade: "))
semestre = int(input("Digite o número do seu semestre: "))
total = 10
print("Você tem",idade,"anos e",total-semestre,"semestres pela frente")
print("Você vai se formar em",(total-semestre)/2,"anos")

#5.5
idade = int(input("\nDigite sua idade: "))
semestre = int(input("Digite o número do seu semestre: "))
atrasado = int(input("Quantos semestres você está atrasado? "))
total = 10
print("Você tem",idade,"anos e já fez",semestre+atrasado,"semestres")
print("Você vai se formar em",(total-semestre)/2,"anos")
