#6.1
nomes = ["Arthur","Gabriel","Matheus","Caio","Kanehira"]
for nome in nomes:
    print(nome)
print("")

#6.2
for nome in nomes:
    print(nome,"é meu amigo")
print("")

#6.3
carros = ["Audi","Volkswagen","Mercedes","Ferrari"]
for carro in carros:
    print("Eu gostaria de ter uma",carro)
