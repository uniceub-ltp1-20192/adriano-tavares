#7.1
convidados = ["Kurt Cobain","Chester Bennington","Chris Cornell","Oliver Sykes"]
for convidado in convidados:
    print(convidado + ", você está convidado")
print("")

#7.2
convidados.append("Corey Taylor")
naoPodeComparecer = convidados.pop(0)
print(naoPodeComparecer,"não pode mais comparecer ao evento")
for convidado in convidados:
    print(convidado + ", você está convidado")
print("")

#7.3
print("Encontrei uma mesa de jantar maior!")
convidados.insert(0,"Pete Wentz")
convidados.insert(len(convidados)//2,"Gerard Way")
convidados.append("Alex Turner")
for convidado in convidados:
    print(convidado + ", você está convidado")
print("")

#7.4
print("Aconteceu um imprevisto e agora só posso convidar duas pessoas :(")
while len(convidados) > 2:
    removido = convidados.pop()
    print(removido + ", me desculpe :(")

for convidado in convidados:
    print(convidado + ", você ainda está convidado :)")

for i in range(len(convidados)):
    del convidados[0]
print("LISTA:",convidados)


    
    
