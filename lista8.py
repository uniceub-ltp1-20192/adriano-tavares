#8.1
lugares = ["New York","Paris","London","Beijing"]
print("Lista original:",lugares)
print("Lista ordenada:",sorted(lugares))
print("Lista original:",lugares)
print("Lista ordenada inversamente:",sorted(lugares,reverse=True))
print("Lista original:",lugares)
lugares.reverse()
print("Inverter lista:",lugares)
lugares.reverse()
print("Inverter de novo:",lugares)
lugares.sort()
print("Ordenar lista:",lugares)
lugares.sort(reverse=True)
print("Ordenar inversamente:",lugares)
print("")

#8.2
convidados = ["Kurt Cobain","Chester Bennington","Chris Cornell","Oliver Sykes"]
print(convidados)
print("Tamanho da lista:",len(convidados))
print("")

convidados.append("Corey Taylor")
print(convidados)
print("Tamanho da lista:",len(convidados))
print("")

convidados.pop(0)
print(convidados)
print("Tamanho da lista:",len(convidados))
print("")

convidados.insert(0,"Pete Wentz")
convidados.insert(len(convidados)//2,"Gerard Way")
convidados.append("Alex Turner")
print(convidados)
print("Tamanho da lista:",len(convidados))
print("")

for i in range(len(convidados)-2):
    convidados.pop(0)
print(convidados)
print("Tamanho da lista:",len(convidados))
print("")

for i in range(len(convidados)):
    del convidados[0]
print(convidados)
print("Tamanho da lista:",len(convidados))
print("")

#8.3
paises = ["Brasil","Estados Unidos","Inglaterra","Canadá"]
print(paises)
paises.append("Japão")
print(paises)
paises.pop(1)
print(paises)
paises.reverse()
print(paises)
paises.reverse()
print(paises)
print(sorted(paises))
print(sorted(paises,reverse=True))
print(paises)
paises.sort()
print(paises)
paises.sort(reverse=True)
print(paises)
paises.insert(0,"China")
print(paises)
print(len(paises))
del paises[3]
print(paises)
paises.remove("Brasil")
print(paises)
