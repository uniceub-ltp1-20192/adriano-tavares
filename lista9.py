#9.1
lista = [1,3,5,7,9]
total = 0
for i in lista:
    total += i
print("TOTAL SOMA:",total)

#9.2
total = 1
for i in lista:
    total *= i
print("\nTOTAL MULTIPLICAÇÃO:",total)

#9.3
menor = lista[0]
for i in lista:
    if i < menor:
        menor = i
print("\nMENOR ELEMENTO:",menor)
print("")

#9.4
batfamilia = ["Batman","Robin Vermelho","Batgirl","Asa Noturna","Capuz Vermelho"]
sidekick = ["Asa Noturna","Robin Vermelho","Capuz Vermelho"]
comum = []

for membro in batfamilia:
    for robin in sidekick:
        if membro == robin:
            comum.append(robin)
print(comum)
print("")

#9.5
batfamilia.sort()
print(batfamilia)
print("")

#9.6
tamanho = []
for robin in sidekick:
    tamanho.append(len(robin))
print(tamanho)
print("")

#9.7
palavra = "pato"
palavras = ["pato","topa","opta","peito","ponto","pura"]

lista = []
for i in palavra:
    if i not in lista:
        lista.append(i)
        
cont = 0
anagramas = []
print("PALAVRA:",palavra)

for palavra in palavras:
    for letra in palavra:
        if letra in lista:
            cont += 1
            if cont == 4:
                anagramas.append(palavra)
        else:
            cont = 0
    cont = 0
    
print("Anagramas:",end=" ")
for anagrama in anagramas:
    print(anagrama,end=" ")
