#include <iostream>
#include <locale.h>
using namespace std;

int main() {
    setlocale(LC_ALL,"Portuguese");
    int num,peso,somaPeso;
    float nota,somaNota,ponderada;

    ponderada = 0;
    somaNota = 0;
    somaPeso = 0;

    cout << "N�mero de notas: ";
    cin >> num;

    for(int i=0;i<num;i++){
        cout << "\nNota: ";
        cin >> nota;
        while(nota<0 || nota>10){
            cout << "NOTA INV�LIDA" << endl << "Nota: ";
            cin >> nota;
        }

        cout << "Peso (1 a 5): ";
        cin >> peso;
        while(peso<1 || peso>5){
            cout << "PESO INV�LIDO" << endl << "Peso: ";
            cin >> peso;
        }

        ponderada += (nota*peso);
        somaNota += nota;
        somaPeso += peso;
    }

    cout << "\nM�dia sem peso: " << somaNota/num << endl;
    cout << "M�dia com peso: " << ponderada/somaPeso << endl;

	return 0;
}
