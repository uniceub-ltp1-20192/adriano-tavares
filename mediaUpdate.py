print("Esse programa calcula a média das notas da turma")
continuar = True
somaNota,somaPeso,num,ponderada = 0,0,0,0

while continuar:
    
    try:
        nota = float(input("\nNota: "))
        while nota<0 or nota>10:
            nota = float(input("ERRO!Digite sua nota: "))
            
    except ValueError:
        nota = float(input("ERRO!Digite sua nota: "))
        
    finally:
        somaNota += nota


    try:
        peso = int(input("Peso: "))
        while peso<1 or peso>5:
            peso = int(input("ERRO! Digite o peso: "))
            
    except ValueError:
        peso = int(input("ERRO! Digite o peso: "))
        
    finally:
        somaPeso += (nota*peso)
        ponderada += peso
    
    check = input("Deseja continuar? (S/N) ").upper()
    if check != 'S':
        continuar = False
    num += 1

print("------------------------")
print("Média sem peso:",somaNota/num)
print("Média com peso:",somaPeso/ponderada)
