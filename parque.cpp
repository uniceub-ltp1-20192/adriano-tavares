/* Na cidade de Canavieiro do Leste, chegou o parque de divers�es Palha�o louco, onde uma das atra��es � a roda gigante.
Na roda gigante, s� podem entrar pessoas que tenham dois sobrenomes, mais de 1,61 m de altura e n�o estejam sozinhas.
Por�m, se a pessoa tiver exatamente 1,50 m e trouxer quatro acompanhantes, pode entrar. Al�m disso, as cabines s�o numeradas.
Se a pessoa entrar na cabine de n�mero 10 e for a cent�sima pessoa a fazer isso, ganha um pr�mio do palha�o louco.
Fornecido o contexto acima, elabore um algoritmo na abordagem top-down que verifique se uma pessoa pode ou n�o entrar
no brinquedo e indique se ela ser� premiada ou n�o. */

#include <iostream>
#include <sstream>
#include <ctime>
#include <cstdlib>
using namespace std;

string askName();
string askHeight();
string askFriends();

bool validateString(string name);
bool validateFloat(string height);
bool validateInt(string friends);

int validateName(string name);
float validateHeight(string height);
int validateFriends(string friends);

bool checkPermission(int name,float height,int friends);
bool checkPrize();

int main() {
    bool check=true,permission=true;
    bool allowed,prize,vstring,vfloat,vint;
    string name,height,friends;
    int vname,vfriends;
    float vheight;

    name = askName();
    vstring = validateString(name);

    height = askHeight();
    vfloat = validateFloat(height);

    friends = askFriends();
    vint = validateInt(friends);

    if( !(vstring && vfloat && vint) ){
        cout << endl << "Invalid data type" << endl;
        check = false;
    }

    if(check){
        vname = validateName(name);
        vheight = validateHeight(height);
        vfriends = validateFriends(friends);
    }

    allowed = checkPermission(vname,vheight,vfriends);
    if(allowed)
        cout << endl << "You can go in!" << endl;
    else
        cout << endl << "Bye bye!" << endl;

    prize = checkPrize();
    if(prize)
        cout << "You got the Crazy Clown prize!!!" << endl;

	return 0;
}


string askName(){
    string name;
    cout << "Please inform your full name: ";
    getline(std::cin,name);
    return name;
}

bool validateString(string name){
    return true;
}

int validateName(string name){
    int num = 0;
    for(int i=0;i<name.length()-1;i++){
        if( name[i] == ' ' && name[i+1] != ' ' )
            num ++;
    }
    return num;
}



string askHeight(){
    string height;
    cout << "Please inform your height in meters: ";
    cin >> height;
    return height;
}

bool validateFloat(string height){
    int i=0;
    while(height[i] != '\0'){
        if(i==1){
            if(!(height[i] == '.' || height[i] == ',')){
                return false;
            }
        }
        else{
            if(!(isdigit(height[i]))){
                return false;
            }
        }
        i++;
    }
    return true;
}

float validateHeight(string height){
    string decimals = "  ";
    int d;
    float i,f,h;

    decimals[0] = height[2];
    decimals[1] = height[3];
    istringstream(decimals) >> d;
    f = d;
    f = d/100;
    istringstream(height) >> i;
    h = i+f;

    return h;
}



string askFriends(){
    string friends;
    cout << "Please inform the amount of people that are with you: ";
    cin >> friends;
    return friends;
}

bool validateInt(string friends){
    int i=0;
    while(friends[i] != '\0'){
        if( !isdigit(friends[i]) )
            return false;
        i++;
    }
    return true;
}

int validateFriends(string friends){
    int f;
    istringstream(friends) >> f;
    return f;
}



bool checkPermission(int name,float height,int friends){
    return (name>=2 && height>1.61 && friends>0) || (height==1.50 && friends==4);
}

bool checkPrize(){
    int cabinMax=10,peopleMax=100;
    int randomCabin,randomPeople;

    srand(time(0));
    randomCabin = (rand () % cabinMax)+1;
    randomPeople = (rand () % peopleMax)+1;

    return (randomCabin==10 && randomPeople==100);
}
