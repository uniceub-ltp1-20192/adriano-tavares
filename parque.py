""" Na cidade de navieiro do leste, chegou o parque de diversões Palhaço louco, onde uma das atrações é a roda gigante.
Na roda gigante, só podem entrar pessoas que tenham dois sobrenomes, mais de 1,61 m de altura e não estejam sozinhas.
Porém, se a pessoa tiver exatamente 1,50 m e trouxer quatro acompanhantes, pode entrar. Além disso, as cabines são numeradas.
Se a pessoa entrar na cabine de número 10 e for a centésima pessoa a fazer isso, ganha um prêmio do palhaço louco.
Fornecido o contexto acima, elabore um algoritmo na abordagem top-down que verifique se uma pessoa pode ou não entrar
no brinquedo e indique se ela será premiada ou não. """

from random import randint

def askName():
    return input("What's your name? ")

def askHeight():
    return input("What's your height in meters? ")

def askFriends():
    return input("How many people are with you? ")

def validateString(name):
    try:
        str(name)
        return True
    except ValueError:
        return False

def validateFloat(height):
    try:
        float(height)
        return True
    except ValueError:
        return False
        
def validateInt(friends):
    try:
        int(friends)
        return True
    except ValueError:
        return False

def validateName(name):
    count = 0
    for x in range(len(name)-1):
        if name[x] == " " and (name[x+1].isdigit() or name[x+1].isalpha()):
            count += 1
    if count >= 2:
        return True

def validateHeight(height):
    h = float(height)
    return not(h < 0.50 or h > 2.30)

def validateFriends(friends):
    f = int(friends)
    return not(f < 0 or f > 20)

def checkPermission(name,height,friends):
    h = float(height)
    f = int(friends)
    return (name and h > 1.61 and f > 0) or (h == 1.50 and f == 4)

def checkPrize():
    cabinNumber = randint(1,10)
    userCount = randint(1,100)
    return (cabinNumber == 10) and (userCount == 100)

def main():
    check = True

    name = askName()
    height = askHeight()
    friends = askFriends()

    vs = validateString(name)
    vf = validateFloat(height)
    vi = validateInt(friends)


    if not(vs and vf and vi):
        print("\nUnavailable data type")
        return not check
    
    if check:
        vname = validateName(name)
        vheight = validateHeight(height)
        vfriends = validateFriends(friends)

    if not(vname and vheight and vfriends):
        print("\nUnavailable information")

    allowed = checkPermission(vname,height,friends)
    if not allowed:
        print("\nBye bye")
    else:
        print("\nYou can go!")

    prize = checkPrize()
    if prize:
        print("\nYou got the Crazy Clown prize!")


main()
