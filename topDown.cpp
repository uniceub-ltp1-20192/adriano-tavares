/* Na cidade de navieiro do leste, chegou o parque de diversões Palhaço louco, onde uma das atrações é a roda gigante. 
Na roda gigante, só podem entrar pessoas que tenham dois sobrenomes, mais de 1,61 m de altura e não estejam sozinhas. 
Porém, se a pessoa tiver exatamente 1,50 m e trouxer quatro acompanhantes, pode entrar. Além disso, as cabines são numeradas. 
Se a pessoa entrar na cabine de número 10 e for a centésima pessoa a fazer isso, ganha um prêmio do palhaço louco. 
Fornecido o contexto acima, elabore um algoritmo na abordagem top-down que verifique se uma pessoa pode ou não entrar 
no brinquedo e indique se ela será premiada ou não. */

#include <iostream>
#include <locale.h>

using namespace std;

int main(){
	setlocale(LC_ALL,"Portuguese");

	string nome;
	int altura,alturaTeste,acompanhantes,acompanhantesTeste;
	bool nomeTeste,cabineTeste,centesimaTeste;

	// nomes = 3 (retorna true);
	cout << "Digite seu nome: ";
	cin >> nome;
	nomeTeste = verificaNome(nome);

	/* 
	altura >  161 cm (retorna 1);
	altura == 150 cm (retorna 2);
	altura >  0 cm   (retorna 3);
	*/
	cout << endl << "Digite sua altura em cm: ";
	cin >> altura;
	alturaTeste = verificaAltura(altura);

	/* 
	acompanhantes == 4 (retorna 1);
	acompanhantes >  0 (retorna 2); 
	*/
	cout << endl << "Digite o número de acompanhantes: ";
	cin >> acompanhantes;
	acompanhantesTeste = verificaAcompanhantes(acompanhantes);

	// cabine == 10 (retorna true);
	cabineTeste = verificaCabine();

	// pessoa == 100 (retorna true);
	centestimaTeste = verificaCentestima();


	if(nomeTeste == true && alturaTeste == 1 && acompanhantesTeste == 2)
		cout << "Você pode entrar na roda gigante!" << endl;

	if(alturaTeste == 2 && acompanhantesTeste == 1)
		cout << "Você pode entrar na roda gigante!" << endl;

	if(cabineTeste == true && centesimaTeste == true)
		cout << "Você ganhou o prêmio!" << endl;
	

	return 0;
}