/*
Linguagem Est�tica vs. Linguagem Din�mica

###Linguagem Est�tica###
Uma linguagem � dita como segura se n�o produzir erros de execu��o que passam despercebidos
e causam comportamento arbitr�rio, seguindo a no��o de que programas bem escritos n�o deviam
dar errado (o que �, ficar preso durante a execu��o). Linguagens est�ticas garantem seguran�a
de digita��o de programas por meio de sistemas de tipo est�tico. Contudo, esses sistemas de
digita��o n�o compilam algumas express�es que n�o produzem qualquer erro de digita��o durante
a execu��o (por exemplo, em .NET e Java n�o � poss�vel passar a mensagem m para uma refer�ncia
Object, apesar do objeto implementar um m�todo public m). Isso acontece porque o sistema de tipo
est�tico requer garantir que express�es compiladas n�o gerem qualquer tipo de erro durante a
execu��o. Digita��o est�tica � centrada em garantir que nenhum erro de digita��o seja produzido
durante a execu��o. Esse � o motivo pelo qual linguagens com digita��o est�tica empregam um
policiamento pessimista a respeito da compila��o do programa. Esse pessimismo causa erros de
compila��o em programas que n�o produzem qualquer erro de execu��o. Apesar do programa n�o
produzir erros durante a execu��o, o sistema de digita��o do C# n�o o reconhece como um
programa de compila��o v�lido. Ao mesmo tempo, linguagens est�ticas tamb�m permitem a execu��o
de programas que devem causar uma execu��o com erros (ex: �ndice do vetor fora do limite ou acesso
nulo do ponteiro).

###Linguagem Din�mica###
A abordagem de linguagens din�micas � o oposto. Em vez de garantir que toda express�o v�lida ser�
executada sem nenhum erro, eles fazem todos os programas sintaticamente v�lidos compil�veis. Essa
� uma abordagem muito otimista que causa um grande n�mero de erros de digita��o durante a execu��o
que devem ter sido detectados durante o tempo de compila��o. Essa situa��o, onde linguagens din�micas
comumente lan�am exce��es durante execu��o, � o que � representado como compil�vel com erros de digita��o
durante a execu��o. Essa abordagem permite muitos erros de digita��o na execu��o, compilando programas
que deviam ter sido identificados como errados estaticamente. Esse programa com erros � compil�vel, apesar
de um sistema est�tico deve identificar esse erro antes de sua execu��o.

###Suporte de ambas abordagens###
A linguagem de programa��o StaDyn performa infer�ncia de digita��o no tipo de compila��o, minimizando a
compatibilidade na regi�o de erros de digita��o na execu��o da linguagem din�mica e a n�o compat�vel e
sem erros na �rea de digita��o na execu��o de linguagens est�ticas. Consequentemente, StaDyn detecta o
erro de compila��o do programa din�mico e compila o c�digo est�tico v�lido usando sua pr�pria sintaxe.
Para ambas aproxima��es de digita��o, usamos a mesma linguagem de programa��o, deixando o programador
mover entre uma otimista, flex�vel e de desenvolvimento r�pido (din�mica) para uma mais robusta e
eficiente (est�tica). Essa mudan�a pode ser feita mantendo o mesmo c�digo fonte, apenas mudando as
configura��es de compila��o. Separamos a preocupa��o do dinamismo (o que �, flexibilidade vs. robustez
e performance) dos requerimentos funcionais da aplica��o (no seu c�digo fonte).
*/

#include <iostream>
using namespace std;

int main() {
    Object[] v = new Object[10];
    for(int i=0;i<10;i++){
        v[i] = "String " + i;
        int length = v[i].length();   // Erro de compila��o
    }

	return 0;
}
