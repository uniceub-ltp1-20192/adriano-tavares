from random import shuffle

suits = ("spades","hearts","diamonds","clubs")
special = ("A","Q","J","K")


def generateDeck():
    cards = []
    x = 0

    for suit in suits:
        for i in range(1,11):
            if i < 2 or i > 7:
                value = special[x]
                x += 1
            else:
                value = str(i)
                
            card = (suit,value)
            cards.append(card)

            if i == 10:
                x = 0

    return cards


def shuffleDeck():
    deck = generateDeck()
    shuffle(deck)
    return deck
    

def main():
    print(shuffleDeck())

main()
